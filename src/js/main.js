/*jshint esversion: 6 */ 
import jQuery from "jquery";
window.$ = window.jQuery = jQuery;

import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import Vue from 'vue/dist/vue';
import Swiper from 'swiper/dist/js/swiper.min';

import VueLazyload from 'vue-lazyload';
import { ENGINE_METHOD_DIGESTS } from "constants";



Vue.use(VueLazyload);
// Vue.use(VShowSlide);

// loads the Icon plugin
UIkit.use(Icons);

Vue.options.delimiters = ['@{', '}'];



Vue.component('outline-input', {
    template: `
        <div class="uk-margin outline-input rel" :class="{ 'focused' : isFocused }">
            <transition name="fade">
                <div v-if="showAlert" class="input-alert">Please enter a valid email address</div>
            </transition>
            <label :for="id">@{label}</label>
            <input v-if="!isTextarea" v-model="value" type="text" @focus="isFocused = true" @blur="isFocused = false" class="uk-input uk-form-large" :id="id" :name="name" :placeholder="placeholder" />
            <textarea v-else class="uk-textarea uk-form-large" @focus="isFocused = true" @blur="isFocused = false" :id="id" :name="name" :placeholder="placeholder"></textarea>
        </div>
    `,
    data: function(){
        return {
            isFocused: false,
            value: '',
            showAlert: false
        }
    },
    methods: {
        validEmail: function(email){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    },
    watch: {
        value: function(value){
            if(this.isEmail)
                this.showAlert = value.length > 3 && !this.validEmail(value);
        }
    },
    props: {
        isEmail: {
            default: false,
            type: Boolean
        },
        isTextarea: {
            default: false,
            type: Boolean
        },
        label: {
            default: 'label',
            type: String
        },
        name: {
            default: 'name',
            type: String
        },
        id: {
            default: 'id',
            type: String
        },
        placeholder: {
            default: '',
            type: String
        }
    }
});


Vue.component('contact-form-small',{
    props: ['csrf'],
    mounted: function(){
        console.log( JSON.parse(this.csrf) );
    },
    methods: {
        handleSubmit: function(){
            var element = this.$el;
            this.$nextTick(function(){
                var data = $(element).serialize();
                console.log(data);
                $.post({
                    url: window.siteUrl,
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function(response) {
                        console.log( response );
                        if (response.success) {
                            
                        } else {
                            // response.error will be an object containing any validation errors that occurred, indexed by field name
                            // e.g. response.error.fromName => ['From Name is required']
                            alert('An error occurred. Please try again.');
                        }
                    }
                });
            });
        }
    },
    template: `
    <form method="post" @submit.prevent="handleSubmit" action="" accept-charset="UTF-8" class="small-contact uk-margin-large-top max-width-medium uk-text-left">
        
        <span v-html="JSON.parse(this.csrf)"></span>

        <input type="hidden" name="action" value="contact-form/send">
        <div class="row">
            <div class="col col--1-of-2">
                <outline-input
                    label="Name"
                    id="from-name"
                    name="fromName"
                ></outline-input>
            </div>
            <div class="col col--1-of-2">
                <outline-input
                    :is-email="true"
                    label="Email"
                    id="from-id"
                    name="fromEmail"
                ></outline-input>
            </div>
            <div class="col col--2-of-2">
                <outline-input
                    :is-textarea="true"
                    name="message"
                    id="message"
                    label="Message"
                ></outline-input>
            </div>
        </div>
        <div class="uk-text-center">
            <button href="" class="uk-button uk-button-default uk-button-large green-button">Submit</button>
        </div>
    </form>
    `
});




Vue.component('service-selector', {
    props: {
        services: {
            default: [
                {
                    serviceName: '',
                    heading: '',
                    subHeading: '',
                    body: '',
                    quote: '',
                    image: ''
                }
            ],
            type: Array
        }
    },
    data: function () {
        return {
            currentService: 0
        }
    }, 
    mounted() {
        console.log(this.services);
    },
    methods: {
        prevService: function(){
            if( this.currentService <= 0 ){
                this.currentService = (this.services.length - 1);
            }else{
                this.currentService--;
            }
            console.log( this.currentService );
        },
        nextService: function(){
            if( this.currentService >= (this.services.length - 1) ){
                this.currentService = 0;
            }else{
                this.currentService++;
            }
            console.log( this.currentService );

        }
    },
    template: `
        <section class="max-width service-selector uk-margin-large-bottom">
            <div class="flex">
                <div class="flex-1 uk-padding">
                    <transition name="fade" mode="out-in">
                        <div v-for="(service, index) in services" v-lazy:background-image="service.image" v-if="index == currentService" :key="service.image" class="service-selector-image"></div>
                    </transition>
                </div>
                <div class="flex-1 flex space-between flex-column uk-padding">
                    <div>
                        <a href="" class="text-link" v-for="(service, index) in services" @click.prevent="currentService=index" :key="index" :class="{ 'selected-service' : currentService == index }"><span>@{service.serviceName}</span></a>
                    </div>
                    <transition name="fade" mode="out-in">
                        <q v-for="(service, index) in services" v-if="currentService == index" :key="index" class="uk-text-lead quotes">@{service.quote}</q>
                    </transition>
                </div>
                <div class="flex-1 uk-padding">

                    <article class="uk-article">
                        <transition name="fade" mode="out-in">
                            <div v-for="(service, index) in services" v-if="currentService == index" :key="index">

                                <h1  class="uk-article-title">@{service.heading}</h1>
                            
                                <p class="uk-text-lead" v-if="service.subHeading.length">@{service.subHeading}</p>
                                
                                <div v-html="service.body"></div>
                                <a :href="service.link" class="nav-link inb rm uk-margin-medium-top">MORE INFO</a>
                                
                            </div>
                            

                        </transition>

                        <div class="flex uk-margin-medium-top space-between">
                            <a href="" @click.prevent="prevService()" uk-icon="icon: chevron-left; ratio: 2"></a>
                            <a href="" @click.prevent="nextService()" uk-icon="icon: chevron-right; ratio: 2"></a>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    `
});


function serviceHeight(){
    var vh = window.innerHeight;
    var sh = document.querySelector('.service-header');
    
    function setHeight(){
        if(sh != null){
            sh.style.height = ( vh - 125 ) + 'px';
        }
    }
    setHeight();
}
serviceHeight();
window.onresize = function(){
    serviceHeight();
};

// var accordRows = document.querySelectorAll('.accord-row');
// var accordRowsAmount = accordRows.length;
// var accordRowsArray = [];
// if( accordRows == undefined ){
//     accordRowsAmount = 0;
// }
// for(var i = 0; i < accordRowsAmount; i++){
//     accordRowsArray[i] = i == 0 ? true : false;
// }

var app = new Vue({
    el: '#app',
    data: {
        menuOpen: false,
        accordRows: [],
        scroll: 0,
        nav: window.nav,
        accordOpen: false,
        message: 'Hello Vue!'
    },
    mounted: function(){
    },
    methods: {
        moreInfo: function(){
            var initScroll = window.pageYOffset;
            var dest = $('.service-header').next('section').offset().top;
            $({ n: initScroll }).animate({ n: dest}, {
                duration: 500,
                step: function(now, fx) {
                    window.scroll(0, now);
                }
            });
        }
    }
});




(function(){
    // Detect request animation frame
    var scroll = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
    // IE Fallback, you can even fallback to onscroll
    function(callback){ window.setTimeout(callback, 1000/60) };
    var i = 0;
    function loop(){
        i++;
        if(i >= 2){
            var top = window.pageYOffset;
            var percentageScrolled = (top / (document.body.scrollHeight - window.innerHeight) * 100);
            var progressEl = document.querySelector('.page-progress');

            if(progressEl != null){
                progressEl.style.width = percentageScrolled + '%';
            }

            //console.log( percentageScrolled );
            
            app.scroll = top;
            i = 0;


        }

        
        
        // Recall the loop
        scroll( loop );
    }

    // Call the loop for the first time
    loop();
})();



var quoteSwiper = new Swiper(
    '.quote-swiper',
    {
        autoHeight: true,
        loop: true,
        navigation: {
            nextEl: '.quote-swiper-right',
            prevEl: '.quote-swiper-left',
        },
    }
);

var $ = window.jQuery;

(function(){
    $('.accord-btn-hold').click(function(){
        //$(this).fadeToggle();
        $('.accord-body').slideUp();
        $('.accord-btn-hold').slideDown('fast');

        $('.accord .row0').removeClass('open-accord');
        $(this).parent().parent().addClass('open-accord');

        $(this).slideUp('fast');
        $(this).parent().parent().find('.accord-body').slideDown();
    });
})();

